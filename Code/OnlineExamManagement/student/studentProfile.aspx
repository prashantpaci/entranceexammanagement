﻿<%@ Page Title="" Language="C#" MasterPageFile="~/student/student.Master" AutoEventWireup="true" CodeBehind="studentProfile.aspx.cs" Inherits="OnlineExamManagement.student.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        .content {
            min-height: 250px;
            padding: 15px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .card-container.card {
            max-width: 1500px;
            padding: 40px 40px;
            margin-top:0px;
        }

        .card {
            background-color: #F7F7F7;
            margin: 0 auto 25px;
            border-radius: 2px;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        }

        .style1 {
            color: #006600;
            font-size: large;
            text-align: center;
            min-height: 1em;
            font-weight: bold;
            font-family: Arial;
            font-size: large;
            margin-left: 0;
            margin-right: 0;
            margin-top: 40px;
            margin-bottom: 20px;
            letter-spacing: 0px;
        }

        hr {
            position: relative;
            top: 20px;
            border: none;
            height: 2px;
            background: black;
            margin-bottom: 50px;
        }

        .btn {
            margin-left: 300px;
            margin-right: 300px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="content">

        <div class=" container">
            <div class="row">
                <div class=" col-md-8 mx-auto">

                    <div class="card card-container ">

                        <div class="card-body p-0 ">



              

               



                            <div class="row">
                                <div class="col">
                                    <p class="style1 m-1 ">Your Profile</p>
                                    <center>
                                        <img src="../Assets/Image%20and%20Icon/240_F_229758328_7x8jwCwjtBMmC6rgFzLFhZoEpLobB6L8.jpg" / width="80px">
                                    </center>
                                    
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col">
                                    <hr />
                                </div>
                            </div>



                            <div class="row">

                                   <div class=" col-md-6">
                                    <label>Registration ID:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox13" placeholder="Application Id" runat="server" ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>

                                   <div class=" col-md-6">
                                    <label>Applicant Name:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox1" placeholder="Enter Full Name" runat="server" ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>
                             
                            </div>

                            <div class="row">
                                   <div class=" col-md-4">
                                    <label>Father's Name:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox2" placeholder="Father's Name" runat="server" ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>


                                <div class=" col-md-4">
                                    <label>Mother's Name:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox3" placeholder="Mother's Name" runat="server" ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>


                                <div class=" col-md-4">
                                    <label>Date of Birth:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox4" placeholder="Date of Birth" runat="server" TextMode="Date" ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                            <!--------------------------------- dropdown list -------------------------------->

                            <div class="row">


                                <div class=" col-md-6">
                                    <label>Gender:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox8" placeholder="Gender" runat="server" ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>

                                <div class=" col-md-6">
                                    <label>Email Address:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox11" placeholder="Email Address:" runat="server" TextMode="Email" required="" ReadOnly="True"></asp:TextBox>

                                    </div>
                                </div>
                            </div>

                            <!--------------------------------- dropdown list -------------------------------->



                            <div class="row">
                                <!--------------------------------- dropdown list -------------------------------->

                                <div class=" col-md-4">
                                    <label>State:</label>
                                    <div class="input-group form-group">
                                        <asp:TextBox CssClass=" form-control" ID="TextBox10" placeholder="State" runat="server" ReadOnly="True"></asp:TextBox>

                                    </div>
                                </div>

                                <!--------------------------------- dropdown list -------------------------------->

                                <div class=" col-md-4">
                                    <label>City:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox5" placeholder="City" runat="server" ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>

                                <div class=" col-md-4">
                                    <label>Pin Code:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox6" placeholder="Pin Code" runat="server" TextMode="Number" ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class=" col-md-12">
                                    <label>Full Address:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox7" placeholder="Full Address:" runat="server" TextMode="MultiLine" ReadOnly="True"></asp:TextBox>

                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class=" col-md-6">
                                    <label>Mobile Number(+91):</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox9" placeholder="Mobile Number(+91)" runat="server" TextMode="Phone" ReadOnly="True"></asp:TextBox>

                                    </div>
                                </div>

                                <div class=" col-md-6">
                                    <label>Exam Center City:</label>
                                    <div class="input-group form-group">
                                        <asp:TextBox CssClass=" form-control" ID="TextBox12" placeholder="Exam Center City" runat="server" ReadOnly="True"></asp:TextBox>

                                    </div>
                                </div>
                            </div>










                            <div class="row ">
                                <div class="col">
                                    <hr />
                                </div>
                            </div>

                        </div>


                    </div>



                </div>
            </div>


            <a href="../Home/Home.aspx" style="color: #28A745; font-weight: bold"><< back to home</a>
        </div>



    </section>
</asp:Content>
