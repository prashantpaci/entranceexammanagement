﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HallTicketStudentID.aspx.cs" Inherits="OnlineExamManagement.HallTicket.HallTicketStudentID" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="../Assets/bootstrape1/css/bootstrap.min.css" rel="stylesheet" />


      <style>
        .content {
            min-height: 250px;
            padding: 15px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .card-container.card {
            max-width: 1500px;
            padding: 40px 40px;
        }

        .card {
            background-color: #F7F7F7;
            margin: 0 auto 25px;
            border-radius: 2px;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        }

        .style1 {
            color: #006600;
            font-size: large;
            text-align: center;
            min-height: 1em;
            font-weight: bold;
            font-family: Arial;
            font-size: large;
            margin-left: 0;
            margin-right: 0;
            margin-top: 40px;
            margin-bottom: 20px;
            letter-spacing: 0px;
        }

       

    
    </style>


</head>
<body>
    <form id="form1" runat="server">
        
         <section class="content mt-5">

        <div class=" container">
            <div class="row">
                <div class=" col-md-4 mx-auto">

                    <div class="card card-container ">

                        <div class="card-body p-0 ">



                            <div class="row">
                                <div class="col">
                                    <p class="style1 m-1 ">Hall Ticket </p>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col">
                                    <hr />
                                </div>
                            </div>

                        

                            <div class="row">
                                <div class=" col">
                                    <label>Applicant Name:</label>
                                    <div class="input-group form-group">

                                        <asp:TextBox CssClass=" form-control" ID="TextBox1" placeholder="Enter StudentID" required="" autofocus="" runat="server"></asp:TextBox>
                                    </div>
                                </div>

                              
                            </div>

                           
                            <hr />

                            <div class="row">


                                <div class=" col-md-12">

                                    <div class="input-group form-group">

                                        <asp:Button class="btn btn-lg btn-success btn-block btn-signin form-control " ID="Button1" runat="server" Text="Summit" OnClick="Button1_Click"   />

                                    </div>

                                </div>
                            </div>


                        </div>


                    </div>



                </div>
            </div>
            <a href="../Home/Home.aspx" style="color: #28A745; font-weight: bold"><< back to home</a>
        </div>



    </section>

    </form>
</body>
</html>
