<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="hallTicket.aspx.cs" Inherits="OnlineExamManagement.Testing" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hall Ticket</title>


    <link href="../Assets/bootstrape1/css/bootstrap.min.css" rel="stylesheet" />

    <script type="text/javascript">
        function fnProgram() {
            window.open('SelectProgram.asp', 'LocateProgram', 'titlebar=no,toolbar=no,scrollbars=yes,status=no,resizable=no,controls=no,topmargin=0,leftmargin=0,width=600,height=450,top=100,left=200');
            return false;
        }

    </script>

    <script src="Assets/bootstrape1/js/jquery-3.5.1.slim.min.js"></script>
    <script src="Assets/bootstrape1/js/popper.min.js"></script>
    <script src="Assets/bootstrape1/js/bootstrap.min.js"></script>

    <style type="text/css">
        .auto-style1 {
            width: 100%;
            border: 1px solid #000000;
        }

        .auto-style5 {
            width: 220px;
            height: 162px;
        }

        .auto-style6 {
            height: 162px;
        }

        .auto-style7 {
            width: 220px;
            height: 60px;
        }

        .auto-style8 {
            height: 60px;
        }

        .auto-style9 {
            width: 220px;
            height: 54px;
        }

        .auto-style10 {
            height: 54px;
        }

        .auto-style11 {
            width: 220px;
            height: 55px;
        }

        .auto-style12 {
            height: 55px;
        }

        .auto-style18 {
            height: 55px;
            width: 238px;
        }

        .auto-style19 {
            height: 54px;
            width: 238px;
        }

        .auto-style20 {
            height: 60px;
            width: 238px;
        }

        .auto-style22 {
            height: 55px;
            width: 393px;
        }

        .auto-style23 {
            height: 54px;
            width: 393px;
        }

        .auto-style24 {
            height: 60px;
            width: 393px;
        }
    </style>
</head>


<body class=" ml-3">
    <form id="form1" runat="server">

        <div class="ml-5 ">
            <h3 class=" text-dark">Presenting Tech University </h3>
            <h5>(For information only)</h5>
            <asp:Label ID="Label1" runat="server"></asp:Label>

        </div>




        <div class=" container bg-light p-5 my-5 ">
            <div class="row ">
                <div class="col">


                    <h3 class="text-center text-info">Presenting Tech University Entrance Exam Hall Ticket </h3>
                    <br />


                    <div class="">
                        <div class="">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <table class="auto-style1" border="3" width="80%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="auto-style11" style="font-weight: 700">&nbsp; StudentID</td>
                                    <td class="auto-style22">&nbsp;
                                        <asp:Label ID="Label2" runat="server"></asp:Label>
                                    </td>
                                    <td class="auto-style18"><strong>&nbsp;Date of Examination&nbsp;</strong></td>
                                    <td class="auto-style12">&nbsp;10-10-2020&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style9" style="font-weight: 700">&nbsp; Student Name</td>
                                    <td class="auto-style23">&nbsp;
                                        <asp:Label ID="Label3" runat="server"></asp:Label>
                                    </td>
                                    <td class="auto-style19"><strong>&nbsp;Reporting Time&nbsp;</strong></td>
                                    <td class="auto-style10">&nbsp; 09 : 00 AM&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style7" style="font-weight: 700">&nbsp; Exam Name</td>
                                    <td class="auto-style24">&nbsp; Presenting Tech University Entrance Exam&nbsp; </td>
                                    <td class="auto-style20"><strong>&nbsp;Gate Closing Time</strong></td>
                                    <td class="auto-style8">&nbsp;09 : 30 AM&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style7" style="font-weight: 700">&nbsp; Exam Center Name</td>
                                    <td class="auto-style24">&nbsp;
                                        <asp:Label ID="Label4" runat="server"></asp:Label>
                                    </td>
                                    <td class="auto-style20"><strong>&nbsp;Exam Duration&nbsp;</strong></td>
                                    <td class="auto-style8">&nbsp;&nbsp;10 : 00 AM &nbsp;To&nbsp; 1 : 00 PM&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style5" style="font-weight: 700">&nbsp; Exam Center Address</td>
                                    <td class="auto-style6" colspan="3">&nbsp;
                                        <asp:Label ID="Label5" runat="server"></asp:Label>
                                        <br />
                                        &nbsp;<asp:Label ID="Label6" runat="server"></asp:Label>
                                        <br />
                                        &nbsp;<asp:Label ID="Label7" runat="server"></asp:Label>
                                        &nbsp;,
                                        <asp:Label ID="Label8" runat="server"></asp:Label>
                                        <br />
                                        &nbsp;<asp:Label ID="Label9" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>

                    <p></p>
                    <p>&nbsp;</p>
                    <div class=" text-danger"><b>You are requested to take print out of this page for Future References.</b> <input type="button" value="Print This Page" onclick="javascript: window.print()">
                        </div>

<!--<p><font size=-30 color=black face=webdings> 06203499455 </font> </p>-->
             <hr/><br/>
<b><u>Instructions Entrace Exam:<br />
                    </u></b>
                   
<ol>
<li>Examinee must be in possession of valid&nbsp; Identity&nbsp; during the examination and should not carry Mobile Phone inside the Examination Hall at the time of Examination.
</li><li>Calculators are allowed for use in the examination only in case if written/permitted on Question Paper.
</li><li>Session Timings are as under:
    <br>Morning Session  (10:00 am to 1:00 pm)
    </li><li>The exact duration of the exam will be as indicated on the question paper.&nbsp;
</li>

</ol>



                    <a href="javascript:history.back()">Back</a>

                </div>

            </div>
        </div>
    </form>
</body>
</html>
