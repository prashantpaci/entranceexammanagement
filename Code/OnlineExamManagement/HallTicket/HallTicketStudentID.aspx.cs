﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineExamManagement.HallTicket
{
    public partial class HallTicketStudentID : System.Web.UI.Page
    {

        string strcon = ConfigurationManager.ConnectionStrings["sqlcon1"].ConnectionString;

        protected bool isvalid;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            HallTicket();

            if (isvalid)
            {
                Response.Redirect("~/HallTicket/hallTicket.aspx");
            }
        }



        protected void HallTicket()
        {
            try
            {
                SqlConnection con = new SqlConnection(strcon);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand(" SELECT Student.studentID , Student.ApplicantName , ExamCenter.centerID ,ExamCenter.C_Name ,  ExamCenter.C_state , ExamCenter.C_city , ExamCenter.C_pincode , ExamCenter.C_Address FROM ExamCenter INNER JOIN Student ON Student.ecentername = ExamCenter.C_Name and studentID = '" + TextBox1.Text.Trim() + "' ",  con);

                SqlDataReader dataReader = cmd.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        Response.Write("<script>alert('login successfully ');</script>");
                        Session["studentID"] = dataReader.GetValue(0);
                        Session["ApplicantName"] = dataReader.GetValue(1);
                        Session["C_Name"] = dataReader.GetValue(3);
                        Session["C_state"] = dataReader.GetValue(4);
                        Session["C_city"] = dataReader.GetValue(5);
                        Session["C_pincode"] = dataReader.GetValue(6);
                        Session["C_Address"] = dataReader.GetValue(7);
                        Session["role"] = "studentID";

                        isvalid = true;

                    }
                }
                else
                {
                    Response.Write("<script>alert('invalid StudentId ');</script>");
                }

            }
            catch (Exception ex)
            {

                Response.Write("<script>alert('" + ex.Message + "');</script>");

            }
        }
    }
}