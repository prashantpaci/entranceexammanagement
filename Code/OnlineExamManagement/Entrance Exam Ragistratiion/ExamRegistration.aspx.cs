﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.ComponentModel.DataAnnotations;

namespace OnlineExamManagement.Entrance_Exam_Ragistratiion
{
    public partial class ExamRegistrtion : System.Web.UI.Page
    {
        string strcon = ConfigurationManager.ConnectionStrings["sqlcon1"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {

               DropDownList3.DataSource = Get_Data( "getExamCenterCity" , null);
                DropDownList3.DataBind();

                ListItem liExamCity = new ListItem("Select Exam City", "-1");
                DropDownList3.Items.Insert(0, liExamCity);


                ListItem liCenterName = new ListItem("Select Center Name", "-1");
                DropDownList4.Items.Insert(0, liCenterName);

                DropDownList4.Enabled = false;
            }
        }


        private DataSet Get_Data(String SPName , SqlParameter SPParameter)
        {
           
                SqlConnection con = new SqlConnection(strcon);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand( SPName , con);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if(SPParameter != null)
                {
                    da.SelectCommand.Parameters.Add(SPParameter);
                }

                DataSet dt = new DataSet();
                da.Fill(dt);

                return dt;

        }


       



        protected void Button1_Click(object sender, EventArgs e)
        {
            //Response.Write("<script>alert('testing');</script>");
            if (CheakStudentExist())
            {
                Response.Write("<script>alert('student already exist with same email id ');</script>");
            }
            else
            {

               NewstudentRegistration();
                FindUserDetails();
                EmailSendtoStudent();

               // using querystring
                Response.Redirect("~/Entrance Exam Ragistratiion/registrationID.aspx?userID=" + TextBox11.Text);

                LoopControls(this.Controls);
            }
        }

        protected bool CheakStudentExist()
        {

          
            try
            {
                SqlConnection con = new SqlConnection(strcon);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand(" SELECT * from Student where email = '"+TextBox11.Text.Trim()+"' ", con);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if(dt.Rows.Count >=1)
                {

                   
                    return true;
                }
                else
                {
                    return false;
                }
              
               


            }
            catch (Exception ex)
            {

                Response.Write("<script>alert('" + ex.Message + "');</script>");
                return false;
            }
        }
        
        
        protected void NewstudentRegistration()
        {
            try
            {
                SqlConnection con = new SqlConnection(strcon);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand("INSERT INTO Student(ApplicantName, FatherName ,motherrName ,  BirthDate  , Gender   , Sstate , city , pincode ,Address, Phone, ecentercity,ecentername,email,password) values(@ApplicantName, @FatherName ,@motherrName ,  @BirthDate  , @Gender   , @Sstate , @city ,@pincode ,@Address, @Phone, @ecentercity,@ecentername,@email,@password)  ", con);

                cmd.Parameters.AddWithValue("@ApplicantName", TextBox1.Text.Trim());
                cmd.Parameters.AddWithValue("@FatherName", TextBox2.Text.Trim());
                cmd.Parameters.AddWithValue("@motherrName", TextBox3.Text.Trim());
                cmd.Parameters.AddWithValue("@BirthDate", TextBox4.Text.Trim());
                cmd.Parameters.AddWithValue("@Gender", DropDownList1.SelectedValue.Trim());
               
                cmd.Parameters.AddWithValue("@Sstate", DropDownList2.SelectedValue.Trim());
                cmd.Parameters.AddWithValue("@city", TextBox5.Text.Trim());
                cmd.Parameters.AddWithValue("@pincode", TextBox6.Text.Trim());
                cmd.Parameters.AddWithValue("@Address", TextBox7.Text.Trim());
                cmd.Parameters.AddWithValue("@Phone", TextBox9.Text.Trim());
                cmd.Parameters.AddWithValue("@ecentercity", DropDownList3.SelectedItem.Text.Trim());
                cmd.Parameters.AddWithValue("@ecentername", DropDownList4.SelectedItem.Text.Trim());
                cmd.Parameters.AddWithValue("@email", TextBox11.Text.Trim());
                cmd.Parameters.AddWithValue("@password", TextBox12.Text.Trim());

                cmd.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('register successfully now go user login to login');</script>");

            }
            catch (Exception ex)
            {

                Response.Write("<script>alert('" + ex.Message + "');</script>");

            }
        }

        private void LoopControls(ControlCollection controlCollection)
        {
            foreach (Control control in controlCollection)
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Text = string.Empty;
                }
                if (control is DropDownList)
                {
                    ((DropDownList)control).SelectedIndex = -1;
                }
                if (control is RadioButton)
                {
                    ((RadioButton)control).Checked = false;
                }
                if (control is CheckBox)
                {
                    ((CheckBox)control).Checked = false;
                }
                if (control.Controls != null)
                {
                    LoopControls(control.Controls);
                }
            }
        }

        protected void FindUserDetails()
        {
            try
            {
                SqlConnection con = new SqlConnection(strcon);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand(" SELECT * from Student where email = '" + TextBox11.Text.Trim() + "' and password = '" + TextBox12.Text.Trim() + "' ", con);

                SqlDataReader dataReader = cmd.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                       
                        Session["Studentid"] = dataReader.GetValue(0);
                        Session["studentname"] = dataReader.GetValue(1);
                        Session["role"] = "studetn";


                       
                    }
                }
                else
                {
                    Response.Write("<script>alert('invalid userId or password');</script>");
                }

            }
            catch (Exception ex)
            {

                Response.Write("<script>alert('" + ex.Message + "');</script>");

            }
        }

        protected void EmailSendtoStudent()
        {
           

            var fromAddress = new MailAddress("prashant88022gupta@gmail.com", "prashant");
            var toAddress = new MailAddress(TextBox11.Text , TextBox1.Text);
            const string fromPassword = "_8802250965p";
            const string subject = "Presenting Tech University Entrance Exam Registered successfully";
            string body = "Dear " + Session["studentname"].ToString() + " " +
                " Your Registration StudentID is " + Session["Studentid"].ToString() + " and password is  " + TextBox12.Text + " check yor hall ticket in your profile by sign in to your account by Email and sent Password with this mail...." +
                "" +
                "" +
                "" +
                "Presenting Tech Univercity Entrance Exam";



            var smtp = new SmtpClient()
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
            };

            try
            {
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
            }
            catch (Exception)
            {

                Response.Write("<script>alert('there is some problem with Email Sending please check your Email And password or enable less secucity app permmision of your Google Account  ');</script>");

            }


        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList3.SelectedIndex == 0)
            {

            }
            else
            {

                DropDownList4.Enabled = true;
                SqlParameter parameter = new SqlParameter("@CityID ", DropDownList3.SelectedValue);

                DataSet DS = Get_Data("getExamCenterNamebyCityID", parameter);

                DropDownList4.DataSource = DS;
                DropDownList4.DataBind();

                ListItem liCenterName = new ListItem("Select Center Name", "-1");
                DropDownList4.Items.Insert(0, liCenterName);

            }
        }
    }
}