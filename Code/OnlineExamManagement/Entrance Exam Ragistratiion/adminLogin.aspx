﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/Entrance Exam Ragistratiion/Ragistration.Master" AutoEventWireup="true" CodeBehind="adminLogin.aspx.cs" Inherits="OnlineExamManagement.Entrance_Exam_Ragistratiion.adminLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        .skin-blue .content-header {
            background: transparent;
        }

        .content-header {
            position: relative;
            padding: 15px 15px 0 15px;
        }

        .box.box-primary {
            border-top-color: #3c8dbc;
        }

        .box {
            position: relative;
            border-radius: 3px;
            background: #ffffff;
            border-top: 3px solid #d2d6de;
            margin-bottom: 20px;
            width: 100%;
            box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        }

        .bg-light {
            background-color: #6a9fd3 !important;
        }

        .content {
            min-height: 250px;
            padding: 15px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .card-container.card {
            max-width: 500px;
            padding: 40px 40px;
        }

        .card {
            background-color: #F7F7F7;
            margin: 0 auto 25px;
            border-radius: 2px;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        }

        .style1 {
            color: #006600;
            font-size: large;
            text-align: center;
            min-height: 1em;
            font-weight:bold;
            font-family: Arial;
            font-size: large;
            margin-left: 0;
            margin-right: 0;
            margin-top: 40px;
            margin-bottom: 20px;
            letter-spacing: 0px;
        }

        .reauth-email {
            display: block;
            color: #404040;
            line-height: 2;
            margin-bottom: 10px;
            font-size: 14px;
            text-align: center;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            box-sizing: border-box;
        }

        a:link {
            text-decoration: none;
        }

        .content-header:hover {
            color: #404040;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section class="content">

        <div  class=" card card-container" style="margin-top: 0px; padding-top: 5px">

            <p  class="style1" >Admin Login </p>

            
            <hr />

            


            <div class="col-md-12 form-group  ">
                    <label for="exampleInputEmail1">Email:</label>


                

                        <asp:TextBox CssClass=" form-control" ID="TextBox1" placeholder="Enter User ID" type="email" required="" autofocus="" runat="server" TextMode="Email"></asp:TextBox>
                    </div>


                
          


            <div class="col-md-12 form-group  ">
               <label for="exampleInputPassword1">Password</label>



                        <asp:TextBox CssClass=" form-control" ID="TextBox2" placeholder="Enter Password" required="" runat="server" TextMode="Password"></asp:TextBox>
                    </div>


                

            <br>




            <hr>


            <div class="form-group">

                <asp:Button class="btn btn-lg btn-success btn-block btn-signin" ID="Button1" runat="server" Text="SIGN IN" OnClick="Button1_Click" />
            </div>

            <br>
            <a id="ctl00_ContentPlaceHolder1_bForgotPassword" class="forgot-password" href="javascript:__doPostBack('ctl00$ContentPlaceHolder1$bForgotPassword','')" style="font-weight: bold; color:red;">Forgot the Password?</a><br>
        </div>
    </section>
</asp:Content>
