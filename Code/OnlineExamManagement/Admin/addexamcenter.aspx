﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="addexamcenter.aspx.cs" Inherits="OnlineExamManagement.Admin.addexamcenter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <style>
            .content {
                min-height: 250px;
                padding: 15px;
                margin-right: auto;
                margin-left: auto;
                padding-left: 15px;
                padding-right: 15px;
            }

            .card-container.card {
                max-width: 1500px;
                padding: 40px 40px;
                margin-top: 20px;
            }

            .card {
                background-color: #F7F7F7;
                margin: 0 auto 25px;
                border-radius: 2px;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            }

            .style1 {
                color: #006600;
                font-size: large;
                text-align: center;
                min-height: 1em;
                font-weight: bold;
                font-family: Arial;
                font-size: large;
                margin-left: 0;
                margin-right: 0;
                margin-top: 40px;
                margin-bottom: 20px;
                letter-spacing: 0px;
            }

       
        hr {
            position: relative;
            top: 20px;
            border: none;
            height: 2px;
            background: black;
            margin-bottom: 50px;
        }

            .btn {
                margin-left: 300px;
                margin-right: 300px;
            }
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class=" container">
        <div class="row">
            <div class=" col-md-12 mx-auto">

                <div class="card card-container ">

                    <div class="card-body p-0 ">



                        <div class="row">
                            <div class="col">
                                <p class="style1 m-1 ">Exam Center Regestration </p>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col">
                                <hr />
                            </div>
                        </div>




                        <div class="row">
                            <div class=" col-md-6">
                                <label>Exam Center Name:</label>
                                <div class="input-group form-group">

                                    <asp:TextBox CssClass=" form-control" ID="TextBox1" placeholder="Enter Full Name Center" required="" autofocus="" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class=" col-md-6">
                                <label>Examination Center In-charge:</label>
                                <div class="input-group form-group">

                                    <asp:TextBox CssClass=" form-control" ID="TextBox2" placeholder="Examination Center In-charge" required="" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>





                        <div class="row">

                            <div class=" col-md-4">
                                <label>Number of Seats:</label>
                                <div class="input-group form-group">

                                    <asp:TextBox CssClass=" form-control" ID="TextBox3" placeholder="Number of Seats" required="" runat="server" TextMode="Number"></asp:TextBox>
                                </div>
                            </div>

                            <div class=" col-md-4">
                                <label>Exam Center City</label>
                                <div class="input-group form-group">
                                    <asp:DropDownList CssClass=" form-control" ID="DropDownList1" runat="server" required="">

                                      <asp:ListItem Text="--Select--" Value="Select" />
                                        <asp:ListItem Text="Delhi" Value="Delhi" />
                                        <asp:ListItem Text="Gurgaon" Value="Gudgaon" />
                                        <asp:ListItem Text="Ghaziabad" Value="Ghaziabad" />
                                        <asp:ListItem Text="Noida" Value="Noida" />

                                    </asp:DropDownList>

                                </div>
                            </div>
                        </div>

                       

                        <div class="row">

                            <div class=" col-md-4">
                                <label>State:</label>
                                <div class="input-group form-group">
                                    <asp:DropDownList CssClass=" form-control" ID="DropDownList2" runat="server" required="">

                                        <asp:ListItem Text="--Select--" Value="Select" />
                                        <asp:ListItem>Andaman and Nicobar Islands</asp:ListItem>
                                        <asp:ListItem>Andhra Pradesh</asp:ListItem>
                                        <asp:ListItem>Arunachal Pradesh</asp:ListItem>
                                        <asp:ListItem>Assam</asp:ListItem>
                                        <asp:ListItem>Bihar</asp:ListItem>
                                        <asp:ListItem>Chandigarh</asp:ListItem>
                                        <asp:ListItem>Chattisgarh</asp:ListItem>
                                        <asp:ListItem>Dadra and Nagar Haveli</asp:ListItem>
                                        <asp:ListItem>Daman and Diu</asp:ListItem>
                                        <asp:ListItem>Delhi</asp:ListItem>
                                        <asp:ListItem>Goa</asp:ListItem>
                                        <asp:ListItem>Gujarat</asp:ListItem>
                                        <asp:ListItem>Haryana</asp:ListItem>
                                        <asp:ListItem>Himachal Pradesh</asp:ListItem>
                                        <asp:ListItem>Jammu and Kashmir</asp:ListItem>
                                        <asp:ListItem>Jharkhand</asp:ListItem>
                                        <asp:ListItem>Karnataka</asp:ListItem>
                                        <asp:ListItem>Kerala</asp:ListItem>
                                        <asp:ListItem>Lakshadweep</asp:ListItem>
                                        <asp:ListItem>Madhya Pradesh</asp:ListItem>
                                        <asp:ListItem>Maharashtra</asp:ListItem>
                                        <asp:ListItem>Manipur</asp:ListItem>
                                        <asp:ListItem>Meghalaya</asp:ListItem>
                                        <asp:ListItem>Mizoram</asp:ListItem>
                                        <asp:ListItem>Nagaland</asp:ListItem>
                                        <asp:ListItem>Orissa</asp:ListItem>
                                        <asp:ListItem>Pondicherry</asp:ListItem>
                                        <asp:ListItem>Punjab</asp:ListItem>
                                        <asp:ListItem>Rajasthan</asp:ListItem>
                                        <asp:ListItem>Sikkim</asp:ListItem>
                                        <asp:ListItem>Tamil Nadu</asp:ListItem>
                                        <asp:ListItem>Tripura</asp:ListItem>
                                        <asp:ListItem>Uttarakhand</asp:ListItem>
                                        <asp:ListItem>Uttaranchal</asp:ListItem>
                                        <asp:ListItem>Uttar Pradesh</asp:ListItem>
                                        <asp:ListItem>West Bengal</asp:ListItem>

                                    </asp:DropDownList>

                                </div>
                            </div>

                            <div class=" col-md-4">
                                <label>City:</label>
                                <div class="input-group form-group">

                                    <asp:TextBox CssClass=" form-control" ID="TextBox5" placeholder="City" required="" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class=" col-md-4">
                                <label>Pin Code:</label>
                                <div class="input-group form-group">

                                    <asp:TextBox CssClass=" form-control" ID="TextBox6" placeholder="Pin Code" required="" runat="server" TextMode="Number"></asp:TextBox>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class=" col-md-12">
                                <label>Full Address:</label>
                                <div class="input-group form-group">

                                    <asp:TextBox CssClass=" form-control" ID="TextBox7" placeholder="Full Address:" runat="server" TextMode="MultiLine" required=""></asp:TextBox>

                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class=" col-md-6">
                                <label>Mobile Number(+91):</label>
                                <div class="input-group form-group">

                                    <asp:TextBox CssClass=" form-control" ID="TextBox9" placeholder="Mobile Number(+91)" runat="server" TextMode="Phone" required=""></asp:TextBox>

                                </div>
                            </div>

                                 <div class=" col-md-6">
                                <label>Email Address:</label>
                                <div class="input-group form-group">

                                    <asp:TextBox CssClass=" form-control" ID="TextBox11" placeholder="Email Address:" runat="server" TextMode="Email" required=""></asp:TextBox>

                                </div>
                            </div>

                        </div>

                         <hr />

                            <div class="row">


                                <div class=" col-md-12">

                                    <div class="input-group form-group">


                                         <asp:Button CssClass="btn btn-lg btn-success btn-block btn-signin form-control " ID="Button2" runat="server" Text="Summit" OnClick="Button2_Click" />

                                    </div>

                                   

                                </div>
                            </div>


                    </div>


                </div>



            </div>
        </div>
        <a href="../Home/Home.aspx" style="color: #28A745; font-weight: bold"><< back to home</a>
    </div>

</asp:Content>
