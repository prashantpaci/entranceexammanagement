﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineExamManagement.Admin
{
    public partial class manageexam : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView1.Visible = true;
                GridView1.DataBind();
            }

        }

        protected void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (Student.Checked == true)
            {
                GridView1.Visible = true;
                GridView2.Visible = false;
                GridView3.Visible = false;
                GridView1.DataBind();
            }
            else if(ExamCenter.Checked==true)
            {
                GridView2.Visible = true;
                GridView1.Visible = false;
                GridView3.Visible = false;
                GridView2.DataBind();
            }
            else
            {
                GridView3.Visible = true;
                GridView1.Visible = false;
                GridView2.Visible = false;
                GridView3.DataBind();
            }
            
        }

        

        // using radiobutton different checked event

       /* protected void Student_CheckedChanged1(object sender, EventArgs e)
        {
            GridView1.Visible = true;
            GridView2.Visible = false;
            GridView3.Visible = false;
            GridView1.DataBind();
        }

        protected void ExamCenter_CheckedChanged1(object sender, EventArgs e)
        {
            GridView2.Visible = true;
            GridView1.Visible = false;
            GridView3.Visible = false;
            GridView2.DataBind();
        }

        protected void Teacher_CheckedChanged(object sender, EventArgs e)
        {
            GridView3.Visible = true;
            GridView1.Visible = false;
            GridView2.Visible = false;
            GridView3.DataBind();
        }*/
    }
}