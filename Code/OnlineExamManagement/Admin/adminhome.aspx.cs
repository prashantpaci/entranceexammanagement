﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineExamManagement.Admin
{
    public partial class adminhome : System.Web.UI.Page
    {

        string strcon = ConfigurationManager.ConnectionStrings["sqlcon1"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            StatusAdmin();
        }

        protected void StatusAdmin()
        {
            try
            {
                SqlConnection con = new SqlConnection(strcon);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd1 = new SqlCommand(" SELECT * from Total_Students ", con);
                SqlCommand cmd2 = new SqlCommand(" SELECT * from Total_ExamCenters ", con);
                SqlCommand cmd3 = new SqlCommand(" SELECT * from  Total_Teacher ", con);

                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);


                DataTable dt1 = new DataTable();
                da1.Fill(dt1);

                DataTable dt2 = new DataTable();
                da2.Fill(dt2);

                DataTable dt3 = new DataTable();
                da3.Fill(dt3);

                Label1.Text = dt1.Rows[0][0].ToString();
                Label2.Text = dt2.Rows[0][0].ToString();
                Label3.Text = dt3.Rows[0][0].ToString();

            }
            catch (Exception ex)
            {

                Response.Write("<script>alert('" + ex.Message + "');</script>");

            }
        }

    }


  
}