﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineExamManagement.Admin
{
    public partial class addexamcenter : System.Web.UI.Page
    {

        string strcon = ConfigurationManager.ConnectionStrings["sqlcon1"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            //Response.Write("<script>alert('register successfully now go user login to login');</script>");
            if(CheakExamCenterExist())
            {
                Response.Write("<script>alert('Center alredy exist with this email ID please try another one');</script>");
            }
            else
            {
                ExamCenterRegistration();
                LoopControls(this.Controls);
            }
        }


        protected bool CheakExamCenterExist()
        {


            try
            {
                SqlConnection con = new SqlConnection(strcon);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand(" SELECT * from ExamCenter where email = '" + TextBox11.Text.Trim() + "' ", con);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }




            }
            catch (Exception ex)
            {

                Response.Write("<script>alert('" + ex.Message + "');</script>");
                return false;
            }
        }


        protected void ExamCenterRegistration()
        {
            try
            {
                SqlConnection con = new SqlConnection(strcon);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand("INSERT INTO ExamCenter(C_Name, C_incharge ,total_seats ,  exam_C_city  , C_state   , C_city , C_pincode , C_Address ,Phone, email ) values(@C_Name, @C_incharge ,@total_seats ,  @exam_C_city  , @C_state   , @C_city , @C_pincode , @C_Address ,@Phone, @email)  ", con);

                cmd.Parameters.AddWithValue("@C_Name", TextBox1.Text.Trim());
                cmd.Parameters.AddWithValue("@C_incharge", TextBox2.Text.Trim());
                cmd.Parameters.AddWithValue("@total_seats", TextBox3.Text.Trim());
                cmd.Parameters.AddWithValue("@exam_C_city", DropDownList1.SelectedValue.Trim());
                cmd.Parameters.AddWithValue("@C_state", DropDownList2.SelectedValue.Trim());
                cmd.Parameters.AddWithValue("@C_city", TextBox5.Text.Trim());
                cmd.Parameters.AddWithValue("@C_pincode", TextBox6.Text.Trim());
                cmd.Parameters.AddWithValue("@C_Address", TextBox7.Text.Trim());
                cmd.Parameters.AddWithValue("@Phone", TextBox9.Text.Trim());
                cmd.Parameters.AddWithValue("@email", TextBox11.Text.Trim());
               

                cmd.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('register successfully');</script>");

            }
            catch (Exception ex)
            {

                Response.Write("<script>alert('" + ex.Message + "');</script>");

            }
        }

        private void LoopControls(ControlCollection controlCollection)
        {
            foreach (Control control in controlCollection)
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Text = string.Empty;
                }
                if (control is DropDownList)
                {
                    ((DropDownList)control).SelectedIndex = -1;
                }
                if (control is RadioButton)
                {
                    ((RadioButton)control).Checked = false;
                }
                if (control is CheckBox)
                {
                    ((CheckBox)control).Checked = false;
                }
                if (control.Controls != null)
                {
                    LoopControls(control.Controls);
                }
            }
        }

    }
}