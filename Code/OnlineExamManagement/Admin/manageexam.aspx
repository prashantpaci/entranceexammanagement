﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="manageexam.aspx.cs" Inherits="OnlineExamManagement.Admin.manageexam" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {

            $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
        });
    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <section class="content ">

        <div class=" container-fluid ">


            <div class="row">
                <div class=" col-md-12 mx-auto">

                    <div class="row">
                        <div class="col text-right mt-3">
                            <a href="../Home/Home.aspx" style="color: #28A745; font-weight: bold"><< back to home</a>

                        </div>
                    </div>


                    <div class="card my-5 border-danger border  ">

                        <div class="card-body  ">



                            <div class="row mb-3 ">
                                <div class="col">
                                    <center>

                                    <p class="style1 m-1 text-success" style="font-size: 25px; font-weight: bold">Select Catagory</p>

                                    </center>

                                </div>
                            </div>



                            <div class="row">
                                <div class="col">
                                    <center style="height: 55px">

                                    <asp:RadioButton CssClass="  font-weight-bold p-2" ID="Student" runat="server" AutoPostBack="True" Checked="True" GroupName="Catagory" Text="Student" value="S" OnCheckedChanged="RadioButton_CheckedChanged"/>&nbsp&nbsp

                                   <asp:RadioButton CssClass="  font-weight-bold p-2" ID="ExamCenter" runat="server" AutoPostBack="True" GroupName="Catagory" Text="Exam Center" value="EC" OnCheckedChanged="RadioButton_CheckedChanged"></asp:RadioButton>&nbsp&nbsp

                                    <asp:RadioButton CssClass="  font-weight-bold p-2" ID="Teacher" runat="server" AutoPostBack="True" GroupName="Catagory" Text="Teacher" value="T" OnCheckedChanged="RadioButton_CheckedChanged"></asp:RadioButton>
                                    </center>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col">
                                    <hr />
                                </div>
                            </div>


                            <div class="row ">
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:oexamDBConnectionString %>" SelectCommand="SELECT * FROM [Student]"></asp:SqlDataSource>
                                <div class="col">
                                    <asp:GridView Class="table table-bordered thead-dark table-striped" ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="studentID" DataSourceID="SqlDataSource1" Visible="False">
                                        <Columns>
                                            <asp:BoundField DataField="studentID" HeaderText="Student ID" InsertVisible="False" ReadOnly="True" SortExpression="studentID" />
                                            <asp:BoundField DataField="ApplicantName" HeaderText="Applicant Name" SortExpression="ApplicantName" />

                                            <asp:BoundField DataField="BirthDate" HeaderText="Birth of Date" SortExpression="BirthDate" />
                                            <asp:BoundField DataField="Gender" HeaderText="Gender" SortExpression="Gender" />
                                            <asp:BoundField DataField="Sstate" HeaderText="State" SortExpression="Sstate" />
                                            <asp:BoundField DataField="city" HeaderText="City" SortExpression="city" />
                                            <asp:BoundField DataField="pincode" HeaderText="Pincode" SortExpression="pincode" />
                                            <asp:BoundField DataField="ecentercity" HeaderText="Exam Center City" SortExpression="ecentercity" />
                                            <asp:BoundField DataField="ecentername" HeaderText="Exam Center Name" SortExpression="ecentername" />
                                            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>


                      

                            <div class="row ">
                                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:oexamDBConnectionString %>" SelectCommand="SELECT * FROM [ExamCenter]"></asp:SqlDataSource>
                                <div class="col">
                                    <asp:GridView Class="table table-bordered thead-dark table-striped" ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" DataKeyNames="centerID" Visible="False">
                                        <Columns>
                                            <asp:BoundField DataField="centerID" HeaderText="centerID" InsertVisible="False" ReadOnly="True" SortExpression="centerID" />
                                            <asp:BoundField DataField="C_Name" HeaderText="Center Name" SortExpression="C_Name" />
                                            <asp:BoundField DataField="C_incharge" HeaderText="Center incharge" SortExpression="C_incharge" />
                                            <asp:BoundField DataField="total_seats" HeaderText="Total Seats" SortExpression="total_seats" />
                                            <asp:BoundField DataField="C_state" HeaderText="State" SortExpression="C_state" />
                                            <asp:BoundField DataField="C_city" HeaderText="City" SortExpression="C_city" />
                                            <asp:BoundField DataField="C_pincode" HeaderText="Pincode" SortExpression="C_pincode" />
                                            <asp:BoundField DataField="C_Address" HeaderText="Address" SortExpression="C_Address" />
                                            <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                                            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>


                                  <div class="row ">
                                      <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:oexamDBConnectionString %>" SelectCommand="SELECT * FROM [Teacher]"></asp:SqlDataSource>
                                <div class="col">
                                    <asp:GridView Class="table table-bordered thead-dark table-striped" ID="GridView3" runat="server" AutoGenerateColumns="False" DataKeyNames="TeacherID" DataSourceID="SqlDataSource3" Visible="False">
                                        <Columns>
                                            <asp:BoundField DataField="TeacherID" HeaderText="TeacherID" InsertVisible="False" ReadOnly="True" SortExpression="TeacherID" />
                                            <asp:BoundField DataField="TName" HeaderText="Name" SortExpression="TName" />
                                            <asp:BoundField DataField="Gender" HeaderText="Gender" SortExpression="Gender" />
                                            <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate" />
                                            <asp:BoundField DataField="e_centercity" HeaderText="exam cente rcity" SortExpression="e_centercity" />
                                            <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                                            <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
                                            <asp:BoundField DataField="Sstate" HeaderText="Sstate" SortExpression="Sstate" />
                                            <asp:BoundField DataField="city" HeaderText="city" SortExpression="city" />
                                            <asp:BoundField DataField="pincode" HeaderText="pincode" SortExpression="pincode" />
                                            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                        </div>


                    </div>



                </div>


            </div>


        </div>



    </section>





















</asp:Content>
