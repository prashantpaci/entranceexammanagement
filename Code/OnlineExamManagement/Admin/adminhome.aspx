﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="adminhome.aspx.cs" Inherits="OnlineExamManagement.Admin.adminhome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <section class="content pt-5">

        <div class=" container ">


            <div class="row">
                <div class=" col-md-8 mx-auto">

                    <div class="card  ">

                        <div class="card-body p-0  ">



                            <div class="row">
                                <div class="col">
                                    <center>

                                    <p class="style1 m-1 text-dark bg-info" style="font-size:25px; font-weight:bold ">Status</p>

                                    </center>
                                </div>
                            </div>

                              <div class="row ">
                                <div class="col">
                                    <hr />
                                </div>
                            </div>


                                 <div class="row">
                                <div class=" col-6">
                                    <p class="style1 m-1 text-primary" style="font-size:20px; font-weight:bold ">&nbsp; Total Students :
                                 
                                        
                                        <asp:Label CssClass=" text-danger" ID="Label1" runat="server" Text="Label"></asp:Label>
                                        
                                    </p>
                                </div>
                            </div>

                                <div class="row ">
                                <div class=" col-6">
                                    <p class="style1 m-1 text-primary" style="font-size:20px; font-weight:bold ">&nbsp; Total Exam Center :
                                 
                                        
                                        <asp:Label CssClass=" text-danger" ID="Label2" runat="server" Text="Label"></asp:Label>
                                        
                                    </p>
                                </div>
                            </div>


                                <div class="row">
                                <div class=" col-6">
                                    <p class="style1 m-1 text-primary" style="font-size:20px; font-weight:bold ">&nbsp; Total Teacher :
                                 
                                        
                                        <asp:Label CssClass=" text-danger" ID="Label3" runat="server" Text="Label"></asp:Label>
                                        
                                    </p>
                                </div>
                            </div>



                            <div class="row ">
                                <div class="col">
                                    <hr />
                                </div>
                            </div>


                        </div>


                    </div>


                     <a href="../Home/Home.aspx" style="color: #28A745; font-weight: bold"><< back to home</a>

                </div>


            </div>


        </div>



    </section>
</asp:Content>
