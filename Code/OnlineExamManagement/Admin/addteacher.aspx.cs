﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineExamManagement.Admin
{
    public partial class addteacher : System.Web.UI.Page
    {
        string strcon = ConfigurationManager.ConnectionStrings["sqlcon1"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            if(CheakTeacherExist())
            {
                Response.Write("<script>alert('Teacher alredy Exist with this Mail ID plase try another one');</script>");

            }
            else
            {
                TeacherRegistration();
                LoopControls(this.Controls);

            }

        }



        protected bool CheakTeacherExist()
        {


            try
            {
                SqlConnection con = new SqlConnection(strcon);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand(" SELECT * from Teacher where email = '" + TextBox11.Text.Trim() + "' ", con);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);    

                if (dt.Rows.Count >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }




            }
            catch (Exception ex)
            {

                Response.Write("<script>alert('" + ex.Message + "');</script>");
                return false;
            }
        }


        protected void TeacherRegistration()
        {
            try
            {
                SqlConnection con = new SqlConnection(strcon);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand cmd = new SqlCommand("INSERT INTO Teacher(TName, Gender ,BirthDate ,  e_centercity  , Phone   , email , Sstate , city ,pincode, Address ) values(@TName, @Gender ,@BirthDate ,  @e_centercity  , @Phone   , @email , @Sstate , @city ,@pincode, @Address )  ", con);

                cmd.Parameters.AddWithValue("@TName", TextBox1.Text.Trim());
                cmd.Parameters.AddWithValue("@BirthDate", TextBox4.Text.Trim());
                cmd.Parameters.AddWithValue("@Gender", DropDownList3.SelectedValue.Trim());
                cmd.Parameters.AddWithValue("@e_centercity", DropDownList1.SelectedValue.Trim());
                cmd.Parameters.AddWithValue("@Sstate", DropDownList2.SelectedValue.Trim());
                cmd.Parameters.AddWithValue("@city", TextBox5.Text.Trim());
                cmd.Parameters.AddWithValue("@pincode", TextBox6.Text.Trim());
                cmd.Parameters.AddWithValue("@Address", TextBox7.Text.Trim());
                cmd.Parameters.AddWithValue("@Phone", TextBox9.Text.Trim());
                cmd.Parameters.AddWithValue("@email", TextBox11.Text.Trim());


                cmd.ExecuteNonQuery();
                con.Close();
                Response.Write("<script>alert('register successfully');</script>");

            }
            catch (Exception ex)
            {

                Response.Write("<script>alert('" + ex.Message + "');</script>");

            }
        }

        private void LoopControls(ControlCollection controlCollection)
        {
            foreach (Control control in controlCollection)
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Text = string.Empty;
                }
                if (control is DropDownList)
                {
                    ((DropDownList)control).SelectedIndex = -1;
                }
                if (control is RadioButton)
                {
                    ((RadioButton)control).Checked = false;
                }
                if (control is CheckBox)
                {
                    ((CheckBox)control).Checked = false;
                }
                if (control.Controls != null)
                {
                    LoopControls(control.Controls);
                }
            }
        }
    }
}