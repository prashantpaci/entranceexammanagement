﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/HomePage.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="OnlineExamManagement.Home.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- bootstrap css --%>

    <link href="../Assets/bootstrape1/css/bootstrap.min.css" rel="stylesheet" />
    <%-- data table css --%>

    <link href="../Assets/data%20table/css/jquery.dataTables.min.css" rel="stylesheet" />
    <%-- font awasome css --%>

    <link href="../Assets/Font%20Awasome/css/all.css" rel="stylesheet" />

    <%-- custome css --%>

    <link href="../Assets/Custom%20css/StyleSheet1.css" rel="stylesheet" />

    <%-- bootstrap js --%>

    <script src="../Assets/bootstrape1/js/jquery-3.5.1.slim.min.js"></script>

    <script src="../Assets/bootstrape1/js/popper.min.js"></script>

    <script src="../Assets/bootstrape1/js/bootstrap.min.js"></script>

    <style>
        #image-text {
            background-image: url('../Assets/Image and Icon/bg-text.jpg');
            background-attachment: fixed;
            background-repeat: no-repeat;
            background-position: 50% 46px;
            background-size: cover;
        }

            #image-text .image-text-heading > h2 {
                color: #fff;
                font-size: 50px;
                font-weight: normal;
                font-family: 'Roboto';
            }

            #image-text .image-text-heading span {
                font-weight: bold;
            }

            #image-text a {
                color: red;
            }

                #image-text a:hover, a:visited {
                    background-color: white;
                }

        .padding-bottom-top-120 {
            padding: 120px 0;
            padding-top: 120px;
            padding-right: 0px;
            padding-bottom: 120px;
            padding-left: 0px;
        }

        .white_border {
            border: 1px solid #fff;
            color: #fff;
        }

        .white_border {
            padding: 12px 30px;
            text-transform: uppercase;
            border-radius: 4px;
            display: inline-block;
        }

        .link_arrow, .white_border {
            font-weight: bold;
            font-size: 14px;
        }

        .top15 {
            margin-top: 15px;
        }

        .padding {
    padding: 50px 0;
    padding-top:300px;
}

        .line_1 {
    width: 200px;
    margin-top: 10px;
}
.line_1, .line_2, .line_3 {
    display: block;
    height: 1px;
    background: #808080;
    position: relative;
}



.color_red {
    color: #ED2A28;
}


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <section id="wellcome" class="padding">
        <div class="container">
            <div class="row" align="left">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <h2 class="text-uppercase">Welcome to <span class="color_red">presenting tech</span></h2>
                    <div class="line_1"></div>
                    <div class="line_2"></div>
                    <div class="line_3"></div>
                    <p class="heading_space" align="justify" style="line-height: 25px;">
                        Presenting tech Institute of Technology &amp; Management was established by a philanthropic society in the name of Lord Krishna Charitable Trust (Regd.) to impart higher education at par with global standards to cater the need of today’s industries and technocrats with sound knowledge and managerial skills. With our strong vision, strategy and organizational leadership we endeavor to build the Institute as a pre - eminent centre in Technology and Management.
                    </p>
                    <div align="left" style="padding: 20px 0px;"><a href="#" class=" btn  btn-danger  bg-danger " role="button">Read More</a></div>
                   
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <h3>News &amp; Events</h3>
                    <marquee scrollamount="2" id="news" onmouseover="javascript:news.stop();" onmouseout="javascript:news.start();" behavior="scroll" direction="up" height="245px" width="100%">
					<ul>
												<li style="padding-top:15px;"><a href="#" class="news">Events &amp; Activities</a></li>
												<li style="padding-top:15px;"><a href="#" class="news">University Exam / Notices</a></li>
												<li style="padding-top:15px;"><a href="#" class="news">Notices (Academics &amp; Admins)</a></li>
												<li style="padding-top:15px;"><a href="#" class="news">Alumni Registration cum Security Refund Request</a></li>
											</ul></marquee>
                </div>
            </div>
        </div>
    </section>

    <section id="image-text" class="padding-bottom-top-120">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="image-text-heading top30 bottom30">
                        <h2 class="bottom40">Admission Open for Session 2020-21<br>
                            <span style="color: #ffd800;">Make Your Future</span> With Us</h2>
                        <a href="../Entrance%20Exam%20Ragistratiion/app-login.aspx" class="link_arrow white_border  top15">Apply For Entrance Exam</a>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
