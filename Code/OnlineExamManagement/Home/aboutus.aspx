﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/HomePage.Master" AutoEventWireup="true" CodeBehind="aboutus.aspx.cs" Inherits="OnlineExamManagement.Home.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .padding{
            padding-top:200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <section id="wellcome" class="padding">
        <div class="container p-5 ">
            <div class="row" align="left">
                <div class="col-xs-12 col-sm-12 col-md-9" id="pagedata">
                    <div>
                        <h1 style="font-size: 28px; color: #ED2A28; font-weight: 500; padding: 0px 0px 20px 0px; margin-bottom: 0; margin-top: 0;">About Institution</h1>
                    </div>
                    <p style="text-align: justify;">Presenting Institute of Technology &amp; Management was established by a philanthropic society in the name of Lord Krishna Charitable Trust (Regd.) to impart higher education at par with global standards to cater the need of today’s industries and technocrats with sound knowledge and managerial skills. With our strong vision, strategy and organizational leadership we endeavor to build the Institute as a pre – eminent centre in Technology and Management.</p>
                    <p style="text-align: justify;">
                        presenting tech is approved by AICTE New Delhi, Govt of Haryana and affiliated to Maharishi Dayanand University Rohtak under the dynamic leadership of Dr Ranjan Aggarwal (Chairman), Shri Vijay Gupta (Vice Chairman) and Er G.K.Sethi (Secretary)<br>
                        In a short span, presenting tech has emerged as a sought-after institution, and is growing in popularity at a fast pace its reputation of being a place offering high quality professional education is spreading in India as well as in other parts of the world. The mission of the college is to impart higher education at par with global standards.
                    </p>
                    <p style="text-align: justify;">The Campus of presenting tech at Bilaspur -Tauru Road, Gurgaon has been designed and constructed bearing in mind the aesthetics and the environment. It provides facilities commensurate with a twenty first century Institute of Higher Learning in an environment which enhances the learning process.</p>
                    <p><strong>Salient features</strong></p>
                    <ul>
                        <li>Prominent Location.</li>
                        <li>Lush Green pollution free, air conditioned 3 academic/study blocks.</li>
                        <li>Functionally efficient facilities.</li>
                        <li>Ultra modern labs and workshops.</li>
                        <li>Well stocked library.</li>
                        <li>Spacious and well designed classrooms and tutorial rooms.</li>
                        <li>Acoustically designed seminar hall with latest electronic audia-visual aids.</li>
                        <li>Separate hostels for boys and girls,with reasonable charges.</li>
                        <li>Vast play grounds including football ground, basket – ball, lawn – tennis and badminton courts.</li>
                        <li>Indoor activity centre.</li>
                        <li>Well equipped canteen.</li>
                        <li>On campus banking facilities.</li>
                        <li>Highly active Career Development Cell.</li>
                        <li>Highly qualified and experienced faculty.</li>
                        <li>Round the clock internet access through dedicated line.</li>
                        <li>Continuous faculty up – gradation programs.</li>
                        <li>Separate power grid along with Generators for 100% back up.</li>
                        <li>Adequate medical facilities.</li>
                        <li>Transport facility from Delhi and NCR.</li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div style="border: 1px solid #CCCCCC; border-radius: 6px; padding: 12px; color: #242424; font-family: Arial; line-height: 25px; font-size: 15px;">
                        <div style="" class="submenupage">
                            <h2 class="subheading">About Us</h2>
                            <div style="padding: 3px 3px 3px 10px;"><a href="#" class="smenu">
                                <li type="square">About Trust</li>
                            </a></div>
                            <div style="padding: 3px 3px 3px 10px;"><a href="#" class="smenu">
                                <li type="square">About Institution</li>
                            </a></div>
                            <div style="padding: 3px 3px 3px 10px;"><a href="#" class="smenu">
                                <li type="square">Mission and Vision</li>
                            </a></div>
                            <div style="padding: 3px 3px 3px 10px;"><a href="#" class="smenu">
                                <li type="square">Messages</li>
                            </a></div>
                            <div style="padding: 3px 3px 3px 10px;"><a href="#" class="smenu">
                                <li type="square">Governing Body</li>
                            </a></div>
                            <div style="padding: 3px 3px 3px 10px;"><a href="#" class="smenu">
                                <li type="square">Academic Advisory Board</li>
                            </a></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
