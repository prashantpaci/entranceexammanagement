GO
create database oexamDB
GO
use oexamDB

go





--------------------------------------------------------
-------------------Creating Tables----------------------


--------------------------------------------------------


create table AdminLogintable
(
	LoginID int identity(101,1) primary key,
	Email varchar(30) not null unique,
	Password varchar(20) not null,
	
)

create table ExamCenterCity
(
	CityID int identity(1,1) primary key,
	CityName varchar(50) not null unique,
)


create table ExamCenterName
(
	CenterID int identity(1,1) primary key,
	CenterName varchar(100) not null unique,
	CityID int foreign key references ExamCenterCity(CityID)
)



create table Student
(
	studentID      int  identity(101,1) primary key,
	ApplicantName  varchar(50) not null,
	FatherName      varchar(50) not null,
	motherrName    varchar(50) not null,
	BirthDate       varchar(50) not null,
	Gender         varchar(50) not null,
	Sstate         varchar(50) not null,
	city          varchar(50) not null,
	pincode       varchar(50) not null,
	Address       varchar(max)not null,
	Phone          varchar(50)not null,
	ecentercity    varchar(50)not null,
	ecentername   varchar(50)not null,
	email          varchar(50) not null unique ,
	password       varchar(50) not null 
)



create table ExamCenter
(
	centerID int  identity(206880221,1) primary key,
	C_Name varchar(50) not null,
	C_incharge varchar(50) not null,
	total_seats varchar(50) not null,
	exam_C_city varchar(50) not null,
	C_state varchar(50) not null,
	C_city varchar(50) not null,
	C_pincode varchar(50) not null,
	C_Address varchar(max)not null,
	Phone varchar(50)not null,
	email varchar(50) not null unique ,
	
)




create table Teacher
(
	TeacherID int  identity(101,1) primary key,
	 TName varchar(50) not null,
	Gender varchar(50) not null,
	BirthDate varchar not null,
	e_centercity varchar(50) not null,
	Phone varchar(50)not null,
	email varchar(50) not null unique ,
	Sstate varchar(50) not null,
	city varchar(50) not null,
	pincode varchar(50) not null,
	Address varchar(max)not null,
	
)




--------------------------------------------------------
-------------------views----------------------


--------------------------------------------------------


create View  Total_Students
AS
select COUNT(studentID) as 'Total Students' from Student


create View  Total_ExamCenters
AS
select COUNT(centerID) as 'Total Exam Center' from ExamCenter


create View  Total_Teacher
AS
select COUNT(TeacherID) as 'Total Teacher' from Teacher






--------------------------------------------------------
-------------------Stored Procedure----------------------


--------------------------------------------------------

create procedure getExamCenterCity
as
begin
     select CityID ,CityName from ExamCenterCity
end


create procedure getExamCenterNamebyCityID 
@CityID int
as
begin
     select CenterID ,CenterName from ExamCenterName

	 where CityID = @CityID
end














SELECT Student.studentID , Student.ApplicantName , ExamCenter.centerID , ExamCenter.C_Name , ExamCenter.C_state , ExamCenter.C_city , ExamCenter.C_pincode , ExamCenter.C_Address FROM ExamCenter
INNER JOIN Student ON Student.ecentername = ExamCenter.C_Name and studentID=206201;
	 																     
																	  
																	  